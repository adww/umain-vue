
import axios from 'axios'

export async function getCompanyByUserId (context, id) {
  const url = process.env.API + 'companies?userId=' + id + '&includes=companyLinks,companyDepartments,companyPhoto,companyType,numberEmployees'
  axios.defaults.headers.common.authorization = sessionStorage.token
  await axios
    .get(url, {
      authorization: sessionStorage.token
    })
    .then(response => {
      context.commit('updateCompanyData', response.data[0])
    })
    .catch(error => {
      if (error.response.status === 401) {
        this.$router.push('/account/signin')
        return
      }
      this.errored = true
      // this.$q.notify(`Erro ${error.response.status}: ${error.response.data.message}`)
    })
}
export async function getCompanyById (context, id) {
  const url = process.env.API + 'companies?id=' + id + '&includes=companyLinks,companyDepartments,companyPhoto,companyType,numberEmployees'
  axios.defaults.headers.common.authorization = sessionStorage.token
  await axios
    .get(url, {
      authorization: sessionStorage.token
    })
    .then(response => {
      context.commit('updateCompanyData', response.data[0])
    })
    .catch(error => {
      if (error.response.status === 401) {
        this.$router.push('/account/signin')
        return
      }
      this.errored = true
      // this.$q.notify(`Erro ${error.response.status}: ${error.response.data.message}`)
    })
}
