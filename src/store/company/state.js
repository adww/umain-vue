export default function () {
  return {
    companyData: {
      id: null,
      userId: null,
      numberEmployees: null,
      tradingName: 'Company trading name',
      legalName: '',
      registryNumber: '',
      active: true,
      companyType: null,
      companyAddress: null,
      about: '',
      socialResponsability: '',
      diversityCommittee: '',
      environmentalResponsibility: '',
      phisicalAndCommunicationalAccessibility: '',
      jobProgression: '',
      flexWorkTime: '',
      flexWorkPlace: '',
      anotherProgramsOrActions: '',
      testimonials: [],
      companyPhoto: [
        {}
      ],
      companyLinks: [],
      companyDepartments: []
    }
  }
}
