
export function updateCompanyData (state, newData) {
  state.companyData = newData
}

export function addLink (state, newLink) {
  state.companyData.companyLinks.push(newLink)
}

export function addDepartment (state, newDepartment) {
  state.companyData.companyDepartments.push(newDepartment)
}

export function setProfileUrl (state, url) {
  state.companyData.images.profileImage.url = (url)
}

export function setProfile (state, newImage) {
  state.companyData.companyPhoto[0] = (newImage)
}

export function setCover (state, newImage) {
  state.companyData.companyPhoto[1] = (newImage)
}

export function setCoverUrl (state, url) {
  state.companyData.images.coverImage.url = (url)
}

export function setProfileDescription (state, description) {
  state.companyData.images.profileImage.description = (description)
}

export function setCoverDescription (state, description) {
  state.companyData.images.coverImage.description = (description)
}

export function setCompanyAddress (state, newAddress) {
  state.companyData.companyAddress = (newAddress)
}

export function updateGenericField (state, newData) {
  const [key, value] = newData
  state.companyData[key.toString()] = value
}
