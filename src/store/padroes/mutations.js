/*
export function someMutation (state) {
}
*/
export function setAncestriesList (state, newList) {
  state.ancestriesOptions = newList
}
export function setGenresList (state, newList) {
  state.genresOptions = newList
}
export function setSexualitiesList (state, newList) {
  state.sexualitiesOptions = newList
}
export function setLocaleOption (state, newLocation) {
  console.log(newLocation)
  state.localeOption = newLocation
}

export function setUserAttValue (state, updatedValue) {
  const { att, newValue } = updatedValue
  state.userProfile[att] = newValue
}
