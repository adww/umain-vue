import axios from 'axios'

export function ListAncestries (context) {
  const url = process.env.API + 'ancestries'
  axios.get(url).then(response => {
    context.commit('setAncestriesList', response.data)
  })
}
export function ListGenres (context) {
  const url = process.env.API + 'genres'
  axios.get(url).then(response => {
    context.commit('setGenresList', response.data)
  })
}
export function ListSexualities (context) {
  const url = process.env.API + 'sexualities'
  axios.get(url).then(response => {
    context.commit('setSexualitiesList', response.data)
  })
}
