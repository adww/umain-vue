export default function () {
  return {
    //
    localeOption: {
      value: 'en-US',
      label: 'English'
    },
    ancestriesOptions: [],
    genresOptions: [],
    sexualitiesOptions: [],
    userProfile: {
      ancestry: null,
      gender: null,
      sexuality: null
    }
  }
}
