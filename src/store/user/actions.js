
import axios from 'axios'

export async function getUser (context, id) {
  const url = process.env.API + 'users/' + id
  axios.defaults.headers.common.authorization = sessionStorage.token
  await axios
    .get(url, {
      authorization: sessionStorage.token
    })
    .then(response => {
      context.commit('updateUser', response.data)
    })
    .catch(error => {
      if (error.response.status === 401) {
        this.$router.push('/account/signin')
        return
      }
      this.errored = true
      this.$q.notify(`Erro ${error.response.status}: ${error.response.data.message}`)
    })
}
