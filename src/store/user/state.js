export default function () {
  return {
    userData: {
      active: true,
      ancestryId: 1,
      birthDate: null,
      birthPlace: null,
      companyAccount: false,
      countryId: 1,
      genderId: 1,
      id: null,
      login: '',
      name: '',
      showInfos: false,
      socialName: null
    }
  }
}
