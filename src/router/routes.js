
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      // PAGES
      { path: 'new-page', component: () => import('pages/NewPage.vue') },
      { path: 'about', component: () => import('pages/About.vue') },
      { path: 'contact', component: () => import('pages/Contact.vue') },
      { path: 'privacy', component: () => import('pages/Privacy.vue') },
      { path: 'terms', component: () => import('pages/TermsService.vue') },
      { path: 'commenting-policy', component: () => import('pages/CommentingPolicy.vue') },
      // CANDIDATES
      { path: 'resume', component: () => import('pages/user/Resume.vue') },
      { path: 'dashboard', component: () => import('pages/user/Dashboard.vue') },
      { path: 'profile', component: () => import('pages/user/Profile.vue') },
      { path: 'settings', component: () => import('pages/user/Settings.vue') },
      { path: 'batch-users', component: () => import('pages/user/BatchUsers.vue') },
      { path: 'candidates', component: () => import('pages/candidate/Candidates.vue') },
      { path: 'public-candidate', component: () => import('pages/candidate/PublicCandidate.vue') },
      // COMPANY
      { path: 'company', component: () => import('pages/company/Company.vue') },
      { path: 'view-company/:id', name: 'view-company', component: () => import('pages/company/CompanyInfo.vue') },
      { path: 'companies', component: () => import('pages/company/Companies.vue') },
      { path: 'my-companies', component: () => import('pages/company/MyCompanies.vue') },
      { path: 'new-company', component: () => import('pages/company/NewCompany.vue') },
      { path: 'update-company', component: () => import('pages/company/UpdateCompany.vue') },
      // BLOG
      { path: 'new-article', component: () => import('pages/blog/NewArticle.vue') },
      { path: 'edit-article/:id', name: 'edit-article', component: () => import('pages/blog/EditArticle.vue') },
      { path: 'post/:id', name: 'post', component: () => import('pages/blog/Post.vue') },
      { path: 'blog', component: () => import('pages/blog/MainBlog.vue') },
      { path: 'blog/category/:id', name: 'category-posts', component: () => import('pages/blog/BlogByCategory.vue') },
      { path: 'blog/user/:id', name: 'user-posts', component: () => import('pages/blog/BlogByUser.vue') },
      { path: 'my-blog', component: () => import('pages/blog/MyBlog.vue') },
      // JOBS
      { path: 'jobs', component: () => import('pages/job/Jobs.vue') },
      { path: 'job', component: () => import('pages/job/Job.vue') },
      { path: 'my-jobs', component: () => import('pages/job/MyJobs.vue') },
      { path: 'new-job', component: () => import('pages/job/NewJob.vue') },
      { path: 'update-job/:id', name: 'update-job', component: () => import('pages/job/UpdateJob.vue') },
      { path: 'add-job', component: () => import('pages/job/AddJob.vue') },
      { path: 'category', component: () => import('pages/job/Category.vue') },
      // ADMIN
      { path: 'report', component: () => import('pages/admin/Report.vue') },
      { path: 'manage-accounts', component: () => import('pages/admin/ManageAccounts.vue') },
      { path: 'site-settings', component: () => import('pages/admin/SiteSettings.vue') },
      // DOCS
      { path: 'invite_users', component: () => import('assets/docs/invite_users.csv') },
      { path: 'teste', component: () => import('pages/blog/teste.vue') },
      { path: 'create_users', component: () => import('assets/docs/create_users.csv') }
    ]
  },
  {
    path: '/account',
    component: () => import('layouts/FullScreen.vue'),
    children: [
      { path: 'forget', component: () => import('pages/account/Forget.vue') },
      { path: 'signin', component: () => import('pages/account/Signin.vue') },
      { path: 'signup', component: () => import('pages/account/Signup.vue') },
      { path: 'logout', component: () => import('pages/account/Logout.vue') }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
